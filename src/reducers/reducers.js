import { combineReducers } from 'redux'
import { ADD_USER } from '../actions/actions'


function users(state= {}, action){
    switch(action.type) {
        case ADD_USER:
            return action.value
        default:
            return state
    }
}


const userApp = combineReducers({
    users
})

export default userApp