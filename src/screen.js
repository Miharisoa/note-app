import React from 'react';
import './screen.css';


class Screen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value : props.texte}
        // this.state = {date: new Date()};
    }

    

    render() {
      return (
        <div className="screen">
          <h2>{this.state.value}</h2>
        </div>
      )
    }
  }
  
  export default Screen;