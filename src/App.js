import React from 'react';
import './App.css';
// import Home from './components/home/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter
} from "react-router-dom";
import Header from './components/Header/Header';
import Univers from './components/Univers';
import { Provider } from 'react-redux';
import userApp from './reducers/reducers';
import { createStore } from 'redux';
import Home from './views/home/Home';
import About from './views/about/About';
import FooterMobile from './components/Footer/FooterMobile';
import Footer from './components/Footer/Footer';



let store = createStore(userApp)

class App extends React.Component {  

  constructor(){
    super();    
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Header />
            <Univers />
            
            <Switch>
              <Route exact path="/" component={withRouter(Home)} />
              <Route exact={true} path="/about" component={withRouter(About)} />     
            </Switch>
            <FooterMobile />
            <Footer />
          </div>
        </Router>
      </Provider>
    )
  }
}

export default App;


// function About() {
//   return <h2>About</h2>;
// }
