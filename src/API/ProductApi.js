// const API_TOKEN = "1a249eccaedea99a9ebb3bbb377b1e23";
// const DOMAIN_URL = "https://api.themoviedb.org/3/"

// export function getFilmsFromApiWithSearchedText(text,page) {
//     const url = DOMAIN_URL+'search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text+'&page='+page;
//     return fetch(url).then((response) => response.json()).catch((error) => console.log('ERREUR GET FILMS : ', error));
// }

const DOMAIN_URL = "https://api.hadeen-place.fr/api/v1/"
// const DOMAIN_URL = "https://apipreprod.hadeen-place.fr/api/v1/"


export function getAllProducts(type) {
    const url = 'accueil/produits/HADEEN?types='+type;

    return fetch(DOMAIN_URL+url).then((response) => response.json()).catch((error) => console.log('ERROR FECTH ALL PRODUCTS : ',error));
}

export function getAllSliderHome() {
    const url = "publicites/search";
    return fetch(DOMAIN_URL+url,{
        method:'POST'
    }).then((response) => response.json()).catch((error) => console.log('ERROR FECTH SLIDERS : ',error));
}

export function getOffreDuMoment() {
    const url = "produits/search";
    var data = new FormData();
    data.append('pagination.par_page', '12');
    return fetch(DOMAIN_URL+url,{
        method:'POST',
        body: data
    }).then((response) => response.json()).catch((error) => console.log('ERROR FECTH OFFRE DU MOMENT : ',error));
}