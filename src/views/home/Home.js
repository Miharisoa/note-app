import React from 'react';
import CarouselHome from '../../components/Carousel/CarouselHome';

import payement from '../../assets/menu/payment.svg'
import eco from '../../assets/menu/eco.svg'
import french from '../../assets/menu/french.svg'
import pme from '../../assets/menu/pme.svg'
import gift from '../../assets/menu/gift.svg'
import ReviewCard from '../../components/ReviewCard';
import Login from '../../components/login/Login'

import HomeCss from './HomeCss.css';
import { Link } from 'react-router-dom';
import { getAllProducts, getAllSliderHome, getOffreDuMoment } from '../../API/ProductApi';
import CarouselPagination from '../../components/Carousel/CarouselPagination';
import AlaUne from '../../components/AlaUne/AlaUne';
import Button from '@material-ui/core/Button';

import Background from '../../assets/Home/heureux.png'; 
import NewCheck from '../../assets/Home/newCheck.png';
import { makeStyles } from '@material-ui/core';

import * as data from './testimonials'
import Carousel from 'react-bootstrap/Carousel'
import Avatar from '@material-ui/core/Avatar';
import PreviousIcon from '@material-ui/icons/NavigateNext'
import NextIcon from '@material-ui/icons/NavigateBefore'

const useStyles = makeStyles((theme) => ({
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
})); 


function MenuStatique(props) {
    return (
        <div>
            <div className="container py-4 d-none d-md-block">
                <div className="row">
                <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '14px', textAlign: 'center' }}>
                    <img src={payement} /> <br />
                PAIEMENT SECURISE
                </div>
                <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '14px', textAlign: 'center' }}>
                    <img src={eco} /> <br />
                LARGE CHOIX DE PRODUITS ÉCO-RESPONSABLES
                </div>
                <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '14px', textAlign: 'center' }}>
                    <img src={gift} /> <br />
                1 COMMANDE = 1 DON</div>
                <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '14px', textAlign: 'center' }}>
                    <img src={pme} /> <br />
                100% TPE ET PME  </div>
                <div className="col" style={{ fontSize: '14px', textAlign: 'center' }}>
                    <img src={french} /> <br />
                100% ENTREPRISES FRANÇAISES</div>
                </div>
            </div>

            <div className="container pt-4 d-block d-md-none">
                <div className="row">
                    <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '10px', textAlign: 'center' }}>
                        <div className="d-flex">
                            <img src={pme} />
                            <span>100% TPE ET PME  </span>
                        </div>
                    </div>
                    <div className="col" style={{ borderRight: 'solid lightgrey 1px', fontSize: '10px', textAlign: 'center' }}>
                        {/* <img src={payement} />
                        PAIEMENT SECURISE */}
                        <div className="d-flex">
                            <img src={payement} />
                            <span>PAIEMENT SECURISE</span>
                        </div>
                    </div>
                    <div className="col" style={{ fontSize: '10px', textAlign: 'center' }}>                       
                        <div className="d-flex">
                            <img src={french} />
                            <span>100% ENTREPRISES FRANÇAISES</span>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    )
}

function Section() {
    return <section style={sectionStyle} className="my-5 py-3 text-center">
        <h3 className="mt-3 col-12 text-center" style={{color:"#345B2B"}}>Créez votre e-boutique, vendez tous vos articles et créations sur Web et Applications</h3>
        <div className="col-12 px-5">
            <div className="d-flex justify-content-around">
                <div>
                    <img src={NewCheck}/>
                    Gratuit
                </div>
                <div>
                    <img src={NewCheck}/>
                    Sans abonnement
                </div>
                <div>
                    <img src={NewCheck}/>
                    Sans engagement
                </div>
            </div>
        </div>
        <Button variant="outlined" className="mt-3">J'EN PROFITE</Button>
    </section>
}


function WhyHadeen() {

    const classes = useStyles()

    return (
        <div className="d-none d-md-block container theysaid">
            <div className="row">
                <div className="col-md-6 col-sm-12 body-containt">
                <h3 className="mb-3 pourquoiClass">POURQUOI HADÉEN-PLACE?</h3>
                <h5 className="text-left pl-2 mb-1 pb-1 title">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hadéen Place est la nouvelle plateforme de vente française.
                    <br />- 100% de nos vendeurs sont des sociétés françaises.
                    <br />- 100% de ces entreprises sont des artisans, auto-entrepreneurs, TPE ou PME
                </h5>

                <h5 className="text-left pl-2 mb-1 pb-1 title">
                    Chaque article correspond au moins à un impact écologique :
                    <ul>
                    <li>- &nbsp; Made in France</li>
                    <li>- &nbsp; Circuit court</li>
                    <li>- &nbsp; Vegan</li>
                    <li>- &nbsp; Bio</li>
                    <li>- &nbsp; 100% recyclable</li>
                    <li>- &nbsp; Éco responsable</li>
                    <li>- &nbsp; Fait main</li>
                    <li>- &nbsp; 100% d’ingrédients naturels</li>
                    <li>- &nbsp; Issu du commerce équitable</li>
                    <li>- &nbsp; À partir de matériaux recyclés</li>
                    <li>- &nbsp; Sans cruauté animale</li>
                    <li>- &nbsp; Zéro déchet</li>
                    <li>- &nbsp; et tant d’autres …..</li>
                    </ul>
                </h5>

                <h5 className="text-left pl-2 mb-1 pb-1 title">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vous pouvez même créer vos listes de naissances, de mariages, d’anniversaires, de Noël …., les partager avec vos proches et recevoir directement chez vous vos cadeaux .
                    <br />
                    Trouvez en un clin d’œil n’importe quel objet du quotidien ou un cadeau original et personnalisable en navigant dans nos univers : économat, hygiène et santé, femme, homme, bébé et enfant, maison et cuisine, loisirs et animaux, brico, jardin et auto-moto, idée cadeaux et DIY. 
                    <br />
                    Chez Hadéen-place il n’y a pas de mauvaise surprise : le prix que vous voyez = le prix que vous payez, pas d’abonnement, pas de frais supplémentaires.
                    <br />
                    Alors qu’attendez-vous pour consommer responsable ?
                </h5>
                <button className="btn">Nous connaître</button>
                </div>
                <div className="col-md-6 col-sm-12 right">
                    <h3 className="mt-5 mb-4">CE QU'ILS DISENT</h3>

                    <div id="carouselAvis" className="carousel slide" data-ride="carousel" data-interval="false">
                        
                        <div className="carousel-inner">
                            {data.testimonials.map((item, index) => <div key={index} className={index === 0 ? "carousel-item active" : "carousel-item"}>
                                
                                <div className="mt-3">
                                    <Avatar alt="Remy Sharp" src={item.avatar} className="m-auto" style={{width:100, height:100}}/>
                                    <h4 className="avatar-detail">{item.author}</h4>                                
                                </div>

                                <div style={{marginTop:'-20px'}}>
                                    <span className="chevron ml-4 text-left">,,</span>
                                    <h5 className="col-10 offset-1">{item.avis}</h5>
                                    <span className="chevron mr-4" style={{float: 'right', marginTop: '-20px'}}>,,</span>
                                </div>
                                
                            </div>)}
                
                        </div>

                        <a className="carousel-control-prev" href="#carouselAvis" role="button" data-slide="prev" style={{color:'black'}}>
                            <NextIcon />
                        </a>
                        <a className="carousel-control-next" href="#carouselAvis" role="button" data-slide="next" style={{color:'black'}}>
                            <PreviousIcon />
                        </a>

                        { data.testimonials.length >1 && <div className="col-12 mt-2">
                            <div className="row m-auto" style={{width: "fit-content"}}>                       
                                
                                <ol className="carousel-indicators m-0" style={{position:'initial', width:'fit-content'}}>
                                    {[...Array(data.testimonials.length).keys()].map((e,i) => <li key={i} data-target="#carouselAvis" data-slide-to={e}></li>)}                           
                                </ol>
                            </div>
                        </div> }
                    </div>


                </div>
            </div>
        </div>
    );
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            sliders: [],
            aLaUnes: [],
            ventesFlash: [],
            offres: []
        }
    }

    componentDidMount() {
        getAllSliderHome().then((data) => {            
            if(data){
                const sliders = data.data.demande_publicites.filter(e => e.slider !== null);
                const aLaUnes = data.data.demande_publicites.filter(e => e.a_la_une !== null);
                this.setState({sliders});
                this.setState({aLaUnes});
                console.log('STATE HOME', this.state.sliders)
            }
        });

        getOffreDuMoment().then((data) => {
            if(data){
                this.setState({offres:data.data.produits});
                console.log(this.state.offres)
            }
        })

        getAllProducts("VENTE_FLASH").then((data) => {
            if(data){
                this.setState({ventesFlash:data.data.produits});
            }
        })
    }

    render() { 
        return ( 
            <div>        
                
                <main>
                    <CarouselHome sliders={this.state.sliders}/>
                    <MenuStatique />

                    {this.state.offres && <CarouselPagination liste={this.state.offres} title={"Offre du moment"} button={"offres"}/>}

                    <div className="container">                        
                        <h2 className="text-center mb-5 d-none d-md-block">A LA UNE</h2>
                        <h5 className="text-center mb-5 d-block d-md-none">A LA UNE</h5>
                        <div className="row">
                            {this.state.aLaUnes.map((e) => <div className="col-6 col-md-4 mb-2 mb-md-0"><AlaUne item={e}/></div>)}
                        </div>
                    </div>

                     <Section />

                    {this.state.offres && <CarouselPagination liste={this.state.ventesFlash} title={"Vente flash"} button={"ventes"}/>} 

                    <WhyHadeen />

                </main>
            </div>
        );
    }
}


var sectionStyle = {
    // width: "100%",
    minHeight: "210px",
    backgroundImage: `url(${Background})`,
    backgroundSize:'cover'
};


 
export default Home;