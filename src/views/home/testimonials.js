import eric from '../../assets/avis/avis_eric.png'
import cave_bubble from '../../assets/avis/cave_bubble.png'
import elodie from '../../assets/avis/avis_elodie.png'
import richard from '../../assets/avis/little_richard_clothes.png'
import andy from '../../assets/avis/avis_andy.png'
import liliane from '../../assets/avis/avis_liliane.png'
import autreRichard from '../../assets/avis/avis_richard.png'

export const testimonials = [
    {
      id: 1,
      avatar: cave_bubble,
      author: 'cave bubble',
      date: '1.7.2020',
      note: 5,
      // tslint:disable-next-line: max-line-length
      avis: 'Site simple à utiliser, j’ai pu vendre mes articles de bébé fait main rapidement. J’ai reçu mon paiement peu de temps après la réception de l’article.'
    },
    {
      id: 2,
      avatar: eric,
      author: 'Eric',
      date: '31.9.2020',
      note: 5,
      avis: 'j’ai trouvé de bonnes idées de cadeau pour le prochain Noel et surtout j’ai pu faire travailler des entreprises françaises.'
    },
    {
      id: 3,
      avatar: elodie,
      author: 'Elodie',
      date: '13.08.2020',
      note: 4,
      avis: 'j’ai pu créer facilement ma liste de naissance et me faire livrer directement chez moi avant la naissance de mon fils.'
    },
    {
      id: 4,
      avatar: richard,
      author: 'Litlle Richard Clothes',
      date: '12.06.2020',
      note: 5,
      // tslint:disable-next-line: max-line-length
      avis: 'je vendais déjà mes produits dans mon local mais avec la crise sanitaire j’ai dû trouver une autre solution de vente rapide et sécurisée. J’ai essayé Hadéen et pour le moment j’en suis ravi'
    },
    {
      id: 5,
      avatar: andy,
      author: 'Andy',
      date: '09.08.2020',
      note: 5,
      avis: 'je voulais absolument faire travailler des entreprises françaises. Je suis ravis de mes achats.'
    },
    {
      id: 6,
      avatar: liliane,
      author: 'Liliane',
      date: '12.05.2020',
      note: 5,
      avis: 'habituée à utiliser d’autres sites comme A****** ici je sais que j’achète a des petites sociétés françaises.'
    },
    {
      id: 7,
      avatar: autreRichard,
      author: 'Richard',
      date: '10.06.2020',
      note: 4,
      // tslint:disable-next-line: max-line-length
      avis: "j’ai fais la surprise à ma femme de lui créer une liste de mariage , je n’en n’avait jamais fait, je ne savais même pas que ça existait. Tout a été très facile , je l’ai crée , je l’ai partagé et surtout nous avons tout eu pour notre mariage. J’y penserai pour nos prochains événements"},
    
]