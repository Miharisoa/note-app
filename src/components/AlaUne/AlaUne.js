import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: window.innerWidth > 500 ? '236px' : '148px',
    // textAlign: 'center'
  },
  button: {
    minWidth:'64px',
    marginTop: window.innerWidth > 500 ? '20%' : '0%',
    backgroundColor: '#345b2b',
    borderColor: 'white',
    color: "white"
  },
  text : {
    fontSize: '20px',
    fontWeight: 700
  }  
}));

export default function ALaUne({item}) {
  
  const classes = useStyles();
  const Background = item.a_la_une.publicice_detail.file.url;
  return (
      <div className={[classes.root]} style={{backgroundImage: `url(${Background})`, backgroundSize:"cover"}}>
          {/* <Button variant="outlined">Default</Button> */}
          <div className="pt-5 text-center">
            <p className={classes.text}>{item.a_la_une.publicice_detail.texte_achrochage}</p>
            <Button variant="contained" className={classes.button}>Default</Button>
            
          </div>            
      </div>
  );
}
