import React, {Component} from 'react';
import KeyPadComponent from './keypad-component';
import ResultComponent from './result-component';

class CalculatorComponent extends Component{

    constructor(props){
        super(props);
        this.state = {
            result: ""
        }
    }

    calculate = () => {
        try {
          this.setState({
            // eslint-disable-next-line
            result: (eval(this.state.result) || "" ) + ""
          })
        } catch (e) {
          this.setState({
              result: "error"
          })
        }
      };
    
      reset = () => {
        this.setState({
            result: ""
        })
      };
    
      backspace = () => {
        this.setState({
            result: this.state.result.slice(0, -1)
        })
      };
    
      onClick =  (button) => {
        switch(button){
          case "=":
            this.calculate();
            break;
          case "C":
            this.reset();
            break;
          case "CE":
            this.backspace();
            break;
          default:
            this.setState({ result: this.state.result + button})
            break;
        }
      }


    render(){
        return (
            <div>
                
                <ResultComponent result={this.state.result}></ResultComponent>
                <KeyPadComponent onClick={this.onClick}></KeyPadComponent>
            </div>
        )
    }
}

export default CalculatorComponent;