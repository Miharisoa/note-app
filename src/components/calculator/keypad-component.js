import React, {Component} from 'react';

class KeyPadComponent extends Component{

    render(){
        return (
            <div className="">
                <div className="ligne">
                    <button name="(" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">(</button>
                    <button name="CE" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">CE</button>
                    <button name=")" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">)</button>
                    <button name="C" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">C</button>
                </div>

                <div className="ligne">
                <button name="1" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">1</button>
                <button name="2" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">2</button>
                <button name="3" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">3</button>
                <button name="+" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">+</button>
                </div>
                <div className="ligne">
                <button name="4" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">4</button>
                <button name="5" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">5</button>
                <button name="6" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">6</button>
                <button name="-" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">-</button>
                </div>   
                <div className="ligne">   
                <button name="7" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">7</button>
                <button name="8" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">8</button>
                <button name="9" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">9</button>
                <button name="*" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">x</button>
                </div>
                <div className="ligne">
                <button name="." onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">.</button>
                <button name="0" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">0</button>
                <button name="=" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">=</button>
                <button name="/" onClick={e => this.props.onClick(e.target.name)} className="btn btn-primary">÷</button>
                </div>
            </div>
        )
    }
}

export default KeyPadComponent;