
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import ReviewCard from "../ReviewCard";
import CarouselCss from './Carousel.css';
import Button from '@material-ui/core/Button';
import PreviousIcon from '@material-ui/icons/NavigateNext'
import NextIcon from '@material-ui/icons/NavigateBefore'

function ButtonAllOffres({text}){
    return <Button variant="outlined" className="mt-5">
        TOUTES NOS {text}
    </Button>
}

export default function({liste, title, button}) {

    const itemPerSlide = 4;
    // const col = window.innerWidth < 500 ? "col-6" : (itemPerSlide === 4 ? "col-3" : "col-4");
    const totalSlide = Math.ceil(liste.length/itemPerSlide);
    let tableau = [];

    for (let index = 0; index < totalSlide; index++) {
        const start = index*itemPerSlide;
        const element = liste.slice(start,start+itemPerSlide);
        tableau.push(element);
    }

    return ( 
        <div className="text-center mt-4 pb-4 container carouselPagination px-0">
            <h2 className="text-center mb-5 d-none d-md-block">{title.toUpperCase()}</h2>
            <h5 className="text-center mb-5 d-block d-md-none">{title.toUpperCase()}</h5>
                
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel" data-interval="false">
                
                <div className="carousel-inner">
                    {tableau.map((slide, index) => <div key={index} className={index === 0 ? "carousel-item active" : "carousel-item"}>
                        <div className="row">
                            {slide.map((item,i) => <div className="col-6 col-md-3 pb-2" key={i}><ReviewCard item={item}/></div>)}
                        </div>
                    </div>)}
        
                </div>
                
                { totalSlide>1 && <div className="col-12 mt-2">
                    <div className="row m-auto" style={{width: "fit-content"}}>                        
                        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style={{position:'initial', width:'fit-content'}}>
                            <NextIcon />                            
                        </a>
                        <ol className="carousel-indicators m-0" style={{position:'initial', width:'fit-content'}}>
                            {[...Array(totalSlide).keys()].map((e,i) => <li key={i} data-target="#carouselExampleIndicators" data-slide-to={e}></li>)}                           
                        </ol>
                        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style={{position:'initial', width:'fit-content'}}>
                            <PreviousIcon />
                        </a>
                    </div>
                </div> }
            </div>

            
            <ButtonAllOffres text={button}/>
            
        </div>
    );
}