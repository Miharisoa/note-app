import React from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
// import { Carousel } from 'react-responsive-carousel';
import Carousel from 'react-bootstrap/Carousel' 
import CarouselCss from './Carousel.css';

class CarouselHome extends React.Component {
    
    constructor(props){
        super(props);
    }

    getColor = (idColor) => {
        const couleurTexte = [
            { id: 1, nom: 'Noir', hexa: '#000000' },
            { id: 2, nom: 'Vert', hexa: '#345B2B' },
            { id: 3, nom: 'Blanc', hexa: '#ffffff' },
            // { id: 11, nom: 'Noir', hexa: '#000000' },
        ];
        return couleurTexte.find(e => e.id === idColor).hexa;
    }

    getStyleButton = (idStyle) => {
        const styleButtons = [
            {
              id : 1,
              nomClasse : 'h-btn-black'
            },
            {
              id : 2,
              nomClasse : 'h-btn-green'
            },
            {
              id : 3,
              nomClasse : 'h-btn-lightgreen'
            },
            {
              id : 4,
              nomClasse : 'h-btn-grey-green'
            },
            {
              id : 5,
              nomClasse : 'h-btn-grey-border-black'
            }
        ];
        return styleButtons.find(e => e.id === idStyle).nomClasse;
    }

    render() { 

        console.log(this.props.sliders)

        const isMobile = window.innerWidth < 500;

        return ( 
            <div id="carouselHome">
                <Carousel>
                    {this.props.sliders.map((slide, cle) => 
                        <Carousel.Item 
                            style={window.innerWidth < 500 ? styles.carouselStyleMobile : styles.carouselStyle} 
                            key={cle} interval={3000}>
                            <img style={window.innerWidth < 500 ? styles.carouselStyleMobile : styles.image}
                                className="d-block w-100"
                                src={slide.slider.publicice_detail.file.url} alt=""/>
                            <Carousel.Caption>
                                <div className={"d-flex flex-column text-"+slide.slider.niveau_affichage.toLowerCase()}>
                                    { isMobile && <h5 style={{color: this.getColor(slide.slider.publicice_detail.couleur_texte)}}>{slide.slider.publicice_detail.texte_achrochage} </h5>}
                                    { !isMobile && <h1 style={{color: this.getColor(slide.slider.publicice_detail.couleur_texte)}}>{slide.slider.publicice_detail.texte_achrochage} </h1>}
                                    {/* <h1 style={{color: this.getColor(slide.slider.publicice_detail.couleur_texte)}}>{slide.slider.publicice_detail.texte_achrochage} </h1> */}
                                    <p className="font-weight-bold" style={{color: this.getColor(slide.slider.couleur_phrase)}}>{slide.slider.phrase_acrochage}</p>
                                    <div className="d-block d-md-none">
                                        <button className={"btn btn-sm "+this.getStyleButton(slide.slider.publicice_detail.style_bouton)} style={{width:'fit-content', position:'initial'}}>
                                            {slide.slider.publicice_detail.nom_bouton}
                                        </button>
                                    </div>
                                    <div className="d-none d-md-block">
                                        <button className={"btn btn-lg "+this.getStyleButton(slide.slider.publicice_detail.style_bouton)} style={{width:'fit-content', position:'initial'}}>
                                            {slide.slider.publicice_detail.nom_bouton}
                                        </button>
                                    </div>                                    
                                </div>
                            </Carousel.Caption>
                        </Carousel.Item >
                    )}
                </Carousel> 

            </div>
        );
    }
}
 
export default CarouselHome;

const styles = {
    carouselStyle:{
        height:'475px'
    },
    image: {
        height:'475px'
    },
    carouselStyleMobile:{
        height:'178px'
    },
}