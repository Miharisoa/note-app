import React from 'react';
import { Link } from 'react-router-dom';
import iconeHeart from '../assets/heart.svg'
import HomeIcon from '@material-ui/icons/Home';

class Univers extends React.Component {
    state = { 
        liste:[
            {title:'Economat',link:'/about'},
            {title:'Hygiène et santé',link:'/'},
            {title:'Femme',link:'/'},
            {title:'Homme',link:'/'},
            {title:'Bébé et enfant',link:'/'},
            {title:'Maison et cuisine',link:'/'},
            {title:'Loisirs et animaux',link:'/'},
            {title:'Brico, jardin et auto-moto',link:'/'},
            {title:'Idées cadeaux et DIY',link:'/'},
        ]
    }
    render() { 
        // <a className="mr-1">| {element.title}</a>
        return ( 
            <div className="container-fluid py-3 d-none d-md-block">
                <div className="text-center">
                    {/* <span><img src={iconeHeart} style={{ marginRight: 5, width:20 }} alt=""/></span> */}
                    <HomeIcon />
                    {this.state.liste.map((element,index) => 
                        <Link to={element.link} className="mr-1 text-dark" key={index}>| {element.title}</Link>
                    )}
                </div>
            </div>
        );
    }
}
 
export default Univers;
