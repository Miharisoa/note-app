import React, { lazy, Suspense } from 'react';

const LazyBouton = lazy(() => import('./Bouton'));

const Bouton = props => (
  <Suspense fallback={null}>
    <LazyBouton {...props} />
  </Suspense>
);

export default Bouton;
