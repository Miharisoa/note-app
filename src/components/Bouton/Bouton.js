import React from 'react';
import PropTypes from 'prop-types';
import styles from './Bouton.module.scss';

const Bouton = () => (
  <div className={styles.Bouton} data-testid="Bouton">
    Bouton Component
  </div>
);

Bouton.propTypes = {};

Bouton.defaultProps = {};

export default Bouton;
