import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Bouton from './Bouton';

describe('<Bouton />', () => {
  test('it should mount', () => {
    render(<Bouton />);
    
    const bouton = screen.getByTestId('Bouton');

    expect(bouton).toBeInTheDocument();
  });
});