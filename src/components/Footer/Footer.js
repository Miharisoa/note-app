// import React from 'react';

import FooterCss from './FooterCss.css'


import Twitter from '@material-ui/icons/Twitter'
import Facebook from '@material-ui/icons/Facebook'
import Instagram from '@material-ui/icons/Instagram'
import CopyRight from '@material-ui/icons/CopyrightTwoTone'

import MasterCard  from '../../assets/card/master card.svg'
import CB  from '../../assets/card/cb.svg'
import Visa  from '../../assets/card/visa.svg'
import AmericanExpress  from '../../assets/card/american-express.png'

import logo1 from '../../assets/logo2.png'


function NewsLetter() {
    return (
        <div className="bg-green">
            <div className="container py-4">
                <div className="row">
                    <div className="col-12 col-md-3" style={{borderRight:'solid white 1px'}}> <h2 className="newsletter text-white">NEWSLETTER</h2> </div>
                    <div className="col-12 col-md-3"> 
                        <h6 className="newsletter text-white">Inscrivez-vous et recevez toutes nos nouveautés et promotions!</h6> 
                    </div>
                    <div className="col-12 col-md-6">                        
                        <div className="input-group mb-3">
                            <input type="text" className="form-control" placeholder="Entrez votre adresse mail" />
                            <div className="input-group-append bg-light">
                                <button className="btn" type="submit">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default function Footer() {

    const services = ['Newsletter', 'CB acceptation', 'Entretien', 'Nos catégories'];
    const espaces = ['Mon compte', 'Espace Vendeurs', 'Contactez - nous'];
    const connaitres = ['Qui sommes-nous', 'CGU - CGV', 'Centre d\'aide'];

    return(
        <footer className="d-none d-md-block" style={{backgroundColor:'#DBFBE4'}}>
            <NewsLetter />
            <div className="container footer py-4">
                <div className="row">
                    <div className="col-md-3">
                        <h5>REJOIGNEZ LA COMMUNAUTÉ</h5>
                        <div className="image-card" style={{display:'inline-block'}}>
                            <a href="https://www.facebook.com/hadeen.place.fr/" target="blank">
                                <Twitter />
                            </a>
                            <a href="https://twitter.com/HadeenPlace" target="blank" >
                                <Facebook />
                            </a>
                            <a href="https:///www.instagram.com/hadeen.place/" >
                                <Instagram />
                            </a>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <h5>ESPACE INFORMATIONS</h5>
                        <ul className="p-0">
                            {espaces.map((e,index)=> <li key={index}> <p className="m-0">{e}</p></li>)}
                        </ul>
                    </div>
                    <div className="col-md-3">
                        <h5>NOUS CONNAÎTRE</h5>
                        <ul className="p-0">
                            {connaitres.map((e,index)=> <li key={index}> <p className="m-0">{e}</p></li>)}
                        </ul>
                    </div>
                    <div className="col-md-3">
                        <h5>PAIEMENT SÉCURISÉ</h5>
                        <div className="image-card" style={{display:'inline-block'}}>
                            <img src={MasterCard} alt="Master Card" style={{width:"55px"}}/>
                            <img src={CB} alt="CB" />
                            <img src={Visa} alt="VISA" className="mx-2"/>
                            <img src={AmericanExpress} alt="" style={{width:"40px"}} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 text-center">
                        <img src={logo1} style={{width:'15%'}}/>
                        <p style={{fontSize:'12px'}}><CopyRight style={{fontSize:'12px'}}/> Copyright 2020 © www.hadeen-place.fr</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}


// style="margin-left: 6px; background-color: #345B2B; padding: 5px 10px; width: fit-content; border-radius: 50%;"
// style="margin-left: 6px; background-color: #345B2B; padding: 5px 7px; width: fit-content; border-radius: 50%;"
// style="margin-left: 6px; background-color: #345B2B; padding: 5px 8px; width: fit-content; border-radius: 50%;"