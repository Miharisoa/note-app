import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FolderIcon from '@material-ui/icons/Folder';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import FavoriteIcon from '@material-ui/icons/FavoriteBorderOutlined';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import SearchIcon from '@material-ui/icons/SearchOutlined'
import UserIcon from '@material-ui/icons/PersonOutline'

const useStyles = makeStyles({
  root: {
    backgroundColor:'#DBFBE4'
  },
});

export default function FooterMobile() {
  const classes = useStyles();
  const [value, setValue] = React.useState('Accueil');

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
      <div className="d-block d-md-none">
        <BottomNavigation value={value} onChange={handleChange} className={classes.root} showLabels>
            <BottomNavigationAction label="Accueil" value="Accueil" icon={<HomeIcon />} />
            <BottomNavigationAction label="Rechercher" value="Rechercher" icon={<SearchIcon />} />
            <BottomNavigationAction label="Favoris" value="Favoris" icon={<FavoriteIcon/>} />
            <BottomNavigationAction label="Compte" value="Compte" icon={<UserIcon />} />
        </BottomNavigation>
      </div>
  );
}
