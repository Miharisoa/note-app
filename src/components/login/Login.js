import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

import { connect } from 'react-redux'
import { addUser } from '../../actions/actions';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
}));

function select(state) {
    return {
       user: state.users
    }
}

function FullWidthTabs(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  const [name, setName] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handleClickInscription = () => {

  }

  const handleChangeTextValue = (event) => {
    setName(event.target.value)
  }

  const handleSaveName = () => {
    console.log('NEW USERNAME',name)
    props.dispatch(addUser({name:name}));
    console.log(props)
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Je suis un nouveau client" {...a11yProps(0)} />
          <Tab label="J'ai déjà un compte" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
            <div className="col-12 mb-4">
                <TextField
                    id="outlined-helperText0"
                    label="Votre nom"
                    onChange={handleChangeTextValue}                    
                    variant="outlined"
                    style={{ borderRadius: 0, width: '100%' }}
                />
            </div>
            <div className="col-12">
                <button className="btn btn-success form-control" 
                    style={{ borderRadius: 0, width:'100%'}}
                    onClick={handleSaveName}>
                    S'inscrire
                </button>
            </div>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
            <div className="text-center">
                <h6>Bonjour</h6>    
                <p>Se connecter avec votre compte</p>
            </div>            
            <div className="col-12 mb-4">
                <TextField
                    id="outlined-helperText"
                    label="Votre email"
                    // defaultValue="Default Value"
                    // helperText="Some important text"
                    variant="outlined"
                    style={{ borderRadius: 0, width: '100%' }}
                />
            </div>
            <div className="col-12 mb-4">
                <TextField
                    id="outlined-helperText"
                    label="Votre mot de passe"
                    type="password"
                    variant="outlined"
                    style={{ borderRadius: 0, width: '100%' }}
                />
            </div>
            <button className="btn btn-success form-control mx-3" style={{ borderRadius: 0, width:'93.5%'}}>Se connecter</button>
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}

export default connect(select)(FullWidthTabs);
