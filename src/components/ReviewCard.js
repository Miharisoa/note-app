import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/FavoriteBorderOutlined';
import ReviewCardCss from './ReviewCardCss.css';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     maxWidth: 345,
//   },
//   media: {
//     height: 0,
//     paddingTop: '56.25%', // 16:9
//   },
//   expand: {
//     transform: 'rotate(0deg)',
//     marginLeft: 'auto',
//     transition: theme.transitions.create('transform', {
//       duration: theme.transitions.duration.shortest,
//     }),
//   },
//   expandOpen: {
//     transform: 'rotate(180deg)',
//   },
//   avatar: {
//     backgroundColor: red[500],
//   },
// }));

export default function ReviewCard({item}) {
  // const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div>
      <span className="love">
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
      </span>
      <div className="card-content">        
        <a>
        <img className="img-product"
          src={item.image}/>
          {/* <div className="top-text">
            <h3>Titre</h3>
          </div> */}
        </a>
      
        <div className="text-center py-2">
          <h4>{item.model.toUpperCase()}</h4>          
          <h5>{item.prix} €</h5>
          {/*style="overflow-wrap: anywhere;" <h5 *ngIf="!data.prix_promotion || data.prix_promotion === 0">{{ data.prix | number : '1.2-2'}} €</h5> */}
        </div>
      </div>
    </div>
  );
}
