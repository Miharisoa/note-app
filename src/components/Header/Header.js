import React from 'react';
import {} from 'react-dom';

import logo1 from '../../assets/logo2.png'
import iconePanier from '../../assets/ic_panier.png'
import iconeHeart from '../../assets/heart.svg'
import iconeAccount from '../../assets/account.svg'
import iconeMenu from '../../assets/ic_menu.png'
import iconeSearch from '../../assets/search.PNG'
import HeaderCss from './Header.css'
import { getAllProducts } from '../../API/ProductApi';
import TemporaryDrawer from '../TemporaryDrawer';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux'

class Header extends React.Component {
    state = { anchorEl:null, open:false, anchorRef:null }

    componentDidMount(){
        console.log('hahaha')
        getAllProducts('VENTE_FLASH').then((data) => {
            console.log('DATA',data)
        })
    }

    handleClick = (event) => {        
        this.setState({anchorEl:event.currentTarget})
    }

    handleClose = () => {        
        this.setState({anchorEl:null})        
    }

    handleToggle = () => {
        const copieState = {...this.state};
        this.setState({open: !copieState.open})
    };

    handleListKeyDown = () => {

    }

    goHome = () => {
        this.props.history.push("/");
    }

    render() { 
        const opened = this.state.anchorEl;
        return ( 
            <header className="container-fluid bg-green-light">
                <nav className="container navbar navbar-expand-sm d-none d-md-block">
                    <div className="row">
                        <div className="col-3">
                            <div className="input-group my-2">
                                <div className="input-group-append">
                                    <button className="btn btn-sm" type="button">
                                        <img src={iconeSearch} style={{ marginLeft: 10 }} alt=""/>
                                    </button>
                                </div>
                                <input type="text" className="bg-green-light form-control-sm border-0" placeholder="Rechercher ici" />

                            </div>
                        </div>
                        <div className="col-6 text-center">
                            <img src={logo1} className="img-logo mb-3" alt="" onClick={this.goHome}/>
                        </div>
                        <div className="col-3 pt-3">

                            <div className="d-flex justify-content-around align-items-center" >
                                <div className="text-center">
                                    <img src={iconeAccount} style={styles.icone} alt="" onClick={this.handleClick}/>
                                    <p style={{ fontSize: '13px', color: 'black' }} onClick={this.handleClick}>
                                        Compte {this.props.user.name}
                                    </p>

                                    <Menu
                                        id="simple-menu"
                                        anchorEl={this.state.anchorEl}
                                        keepMounted
                                        open={Boolean(this.state.anchorEl)}
                                        onClose={this.handleClose}
                                        style={{position:'absolute', top:60}}
                                    >
                                        <MenuItem><span style={{fontWeight:'bold'}}>Bonjour, Identifiez-vous</span></MenuItem>
                                        {/* <MenuItem onClick={this.handleClose}>Je suis nouveau client</MenuItem>
                                        <MenuItem onClick={this.handleClose}>J'ai déjà un compte</MenuItem> */}
                                        
                                        <MenuItem><TemporaryDrawer type="Je suis nouveau client" close={this.handleClose} /></MenuItem>
                                        <MenuItem><TemporaryDrawer type="J'ai déjà un compte" close={this.handleClose}/></MenuItem>
                                        
                                    </Menu>

                                </div>

                                <div>
                                    <img src={iconeHeart} style={styles.icone} alt=""/>
                                    <p style={{ fontSize: '13px', color: 'black' }}>Favoris</p>
                                </div>
                                <div>
                                    <img src={iconePanier} style={styles.icone} alt=""/>
                                    <p style={{ fontSize: '13px', color: 'black' }}>Panier</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </nav>

                <div className="d-block d-md-none bg-green-light py-3 fixed-top">
                    <div className="row px-3 px-md-0">
                        <div className="col-3">
                            <div className="d-flex">
                            {/* <img src={iconeMenu} style={{ width: 20, height: 20 }} alt=""/> */}
                                <TemporaryDrawer type="icone" close={null}/>
                                <img src={iconeSearch} style={{ marginLeft: 10 }} alt=""/>
                            </div>
                        </div>
                        <div className="col-6 text-center">
                            <img src={logo1} className="img-logo" alt="" onClick={this.goHome}/>
                        </div>
                        <div className="col-3">
                            <div className="row">
                                <div className="col-6"><img src={iconeHeart} style={styles.icone} alt=""/></div>
                                <div className="col-6"><img src={iconePanier} style={styles.icone} alt=""/></div>
                            </div>
                        </div>
                    </div>
                </div>


            </header>
        );
    }
}

function select(state) {
    return {
       user: state.users
    }
}
 
export default connect(select)(withRouter(Header));

const styles = {
    icone:{
        width:30,
        height:30
    }
}